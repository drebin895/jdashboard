FLUSH PRIVILEGES;
DROP USER IF EXISTS 'mydjangouser';
CREATE USER 'mydjangouser' IDENTIFIED BY 'qwerty12345';
GO

DROP DATABASE IF EXISTS mydjangoapp;
CREATE DATABASE mydjangoapp;
GO

USE mydjangoapp;
GO

GRANT ALL privileges ON `mydjangoapp`.* TO 'mydjangouser' IDENTIFIED BY 'qwerty12345';
GO
