from django.shortcuts import render
from dashboard.models import MData
from django.views import generic
from .tables import MDataTable
from django_tables2 import SingleTableView
from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from rest_framework.decorators import api_view, permission_classes
import datetime

from .serializers import MDataSerializer
from .models import MData



# Create your views here.
def index(request):
    num_data = MData.objects.all().count()

    context = {
	'num_data': num_data,
    }

    return render(request, 'index.html', context=context)


class MDataViewSet(viewsets.ModelViewSet):
    queryset = MData.objects.all().order_by('mnum')
    serializer_class = MDataSerializer

    @api_view(['PATCH'])
    def patch(self, request, pk):
        mdata_object = MData.objects.get(pk=pk)
        serializer = MDataSerializer(mdata_object, data=request.data, partial=True) # set partial=True to update a data partially
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(code=201, data=serializer.data)
        elif serializer.data["act_end"] == "":
            serializer.data["act_end"] = None
            serializer.save()
            return JsonResponse(code=201, data=serializer.data)
        else:
            return JsonResponse(code=400, data="wrong parameters")

class MDataViewSetToday(viewsets.ModelViewSet):
    queryset = MData.objects.filter(sch_start__gt=str(datetime.date.today()))
    serializer_class = MDataSerializer


class MDataListView(SingleTableView):
    model = MData
    table_class = MDataTable
    template_name = "dashboard/mdata_list.html"
    #paginate_by = 10

class MDataDetailView(generic.DetailView):
    model = MData
    template_name = "dashboard/mdata_detail.html"
    fields = ("__all__")

class MDataEditView(generic.UpdateView):
    model = MData
    template_name = "dashboard/mdata_edit.html"
    fields = ("__all__")
    success_url = '/mdata/'