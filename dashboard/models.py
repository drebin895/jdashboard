from django.db import models
from django.urls import reverse
from django.contrib import admin
from datetime import datetime
from import_export import resources, fields, widgets
from import_export.admin import ImportExportModelAdmin



# Create your models here.
class MData(models.Model):

# Fields
    mnum = models.IntegerField(blank=True, help_text='Enter MNum')
    proj = models.TextField(blank=True, max_length=20, help_text='Enter project')
    sch_start = models.DateTimeField(blank=True, help_text='Enter scheduled start time', null=True, default=None)
    sch_end = models.DateTimeField(blank=True, help_text='Enter scheduled end time', null=True, default=None)
    act_start = models.DateTimeField(blank=True, help_text='Enter actual start time', null=True, default=None)
    act_end = models.DateTimeField(blank=True, help_text='Enter actual end time', null=True, default=None)
    team = models.TextField(blank=True, max_length=10, help_text='Enter team', null=True)

    INFRAX_CHOICE = (
	('a' , "a"),
	('b' , "b"),
	('c' , "c"),
    )

    infrax = models.CharField(
	max_length=1,
	choices=INFRAX_CHOICE,
	blank=True,
	help_text='Choose infrax',
    )
    
    LOC_CHOICE = (
	('j-m' , "j-m"),
	('j-h' , "j-h"),
	('j-l' , "j-l"),
    ('j-t' , "j-t"),
    )

    location = models.CharField(
	max_length=3,
	choices=LOC_CHOICE,
	blank=True,
	help_text='Choose location',
    )

    STATUS_CHOICE = (
	('Scheduled' , "Scheduled"),
	('On-Call' , "On-Call"),
	('Started' , "Started"),
    ('Finished' , "Finished"),
    ('Cancelled' , "Cancelled"),
    )

    status = models.CharField(
	max_length=10,
	choices=STATUS_CHOICE,
	blank=True,
	help_text='Choose status',
    )

    extensions = models.IntegerField(blank=True, help_text='Enter extensions', default=0)


    # Methods
    def get_absolute_url(self):
        """Returns the url to access a particular instance of MyModelName."""
        return reverse('mdata-view', args=[str(self.mnum)])
    
    def __str__(self):
        """String for representing the MyModelName object (in Admin site etc.)."""
        return str(self.mnum)
    
    def start(self):
        self.act_start = datetime.now

class MDataResource(resources.ModelResource):
    mnum = fields.Field(attribute='mnum',
                         column_name='mnum')
    proj = fields.Field(attribute='proj',
                             column_name='proj')
    sch_start = fields.Field(attribute='sch_start',
                             column_name='sch_start', widget=widgets.DateTimeWidget())
    sch_end = fields.Field(attribute='sch_end',
                             column_name='sch_end', widget=widgets.DateTimeWidget())  
    team = fields.Field(attribute='team',
                             column_name='team')
    infrax = fields.Field(attribute='infrax',
                             column_name='infrax')
    location = fields.Field(attribute='location',
                             column_name='location')
    status = fields.Field(attribute='status',
                             column_name='status')

    class Meta:
        model = MData
        import_id_fields = ('mnum',)
        fields = ('mnum' ,'proj','sch_start','sch_end','team','infrax' ,'location' ,'status')
        export_order = fields