/* global Dashboard */

$(document).ready(function () {
    $.ajax({ 
        type: 'GET', 
        url: 'http://10.0.0.36:8000/dashboard/mdata-api-today/', 
        data: { get_param: 'value' },
        datatype: "json",
        success: function (data) { 
            var parsedJson = JSON.stringify(data)
            $.each(data, function(i, item) {
                var bgcolor = '';
                if(data[i].status == "Finished"){
                    bgcolor = 'Tomato';
                }
                else if (data[i].status == "Started"){
                    bgcolor = 'MediumSeaGreen';
                }
                else if (data[i].status == "Cancelled"){
                    bgcolor = 'Orange';
                }
                else if (data[i].status == "Scheduled"){
                    bgcolor = 'Gray';
                }

                dashboard.addWidget('test' + data[i].mnum, 'List', {
                    color: bgcolor,
                    getData: function () {
                        $.extend(this.scope, {
                            title: 'MNUM: ' + data[i].mnum,
                            updated_at: 'Last updated at ' + Date.now(),
                            data: [{label: "Project", value: data[i].proj},
                                {label: "sch_start", value: data[i].sch_start},
                                {label: "sch_end", value: data[i].sch_end},
                                {label: "act_start", value: data[i].act_start},
                                {label: "act_end", value: data[i].act_end},
                                {label: "team", value: data[i].team},
                                {label: "infrax", value: data[i].infrax},
                                {label: "location", value: data[i].location},
                                {label: "status", value: data[i].status},
                                {label: "extensions", value: data[i].extensions}]
                            });
                    }
                });
            });
            
        }
    });
});

var dashboard = new Dashboard();

dashboard.addWidget('clock_widget', 'Clock');



/*
dashboard.addWidget('convergence_widget', 'Graph', {
    getData: function () {
        $.extend(this.scope, {
            title: 'Convergence',
            value: Math.floor(Math.random() * 50) + 40,
            more_info: '',
            data: [
                    { x: 0, y: Math.floor(Math.random() * 50) + 40 },
                    { x: 1, y: Math.floor(Math.random() * 50) + 40 },
                    { x: 2, y: Math.floor(Math.random() * 50) + 40 },
                    { x: 3, y: Math.floor(Math.random() * 50) + 40 },
                    { x: 4, y: Math.floor(Math.random() * 50) + 40 }
                ]
            });
    }
});
*/
