import django_tables2 as tables
from django_tables2.utils import A
from .models import MData

def set_mnum_attr(record):
    if record.status == 'Started':
        return 'bg-success'
    elif record.status == 'Finished':
        return 'bg-danger'
    elif record.status == 'Cancelled':
        return 'bg-warning'

'''
def status_started_attr(record):
    return 'bg-danger' if record.status == 'Started' or record.status == 'Finished' else 'bg-success'

def status_finished_attr(record):
    return 'bg-danger' if record.status == 'Finished' or record.status == 'Scheduled' else 'bg-success'

def status_cancelled_attr(record):
    return 'bg-danger' if record.status == 'Cancelled' or record.status == 'Started' or record.status == 'Finished' else 'bg-success'
'''

class MDataTable(tables.Table):
    T0 = '{{record.mnum}}'
    T1 = '<a class="btn btn-info" role="button" href="{% url "mdata-edit" record.id %}">update</a>'
    T2 = '<a class="start_mission btn btn-success" role="button" data-record-id="{{record.id}}">start</a>'
    #T2 = '<button type="button" class="button btn-sm btn-success" update-link="{{ record.get_absolute_url_update}}">start</button>'
    T3 = '<a class="stop_mission btn btn-danger" role="button" data-record-id="{{record.id}}">stop</a>'
    #'<button type="button" class="button btn-sm btn-danger" update-link="{{ record.get_absolute_url_update }}">stop</button>'
    T4 = '<a class="cancel_mission btn btn-warning" role="button" data-record-id="{{record.id}}">cancel</a>'
    #'<button type="button" class="button btn-sm btn-warning" update-link="{{ record.get_absolute_url_update }}">cancel</button>'
    T5 = '<a class="extend_mission btn btn-primary" role="button" data-record-id="{{record.id}}" data-extensions="{{record.extensions}}">extend</a>'
    #'<button type="button" class="button btn-sm btn-primary" update-link="{{ record.get_absolute_url_update }}">extend</button>'

    mnum = tables.TemplateColumn(T0, orderable=True, attrs={'td': {'class' : set_mnum_attr}})
    edit = tables.TemplateColumn(T1, orderable=False)
    #start = tables.LinkColumn('start_mission', args=[A('mnum')], attrs={'a': {'class': 'btn btn-success'}}, orderable=False, empty_values=())
    start = tables.TemplateColumn(T2, orderable=False)
    stop = tables.TemplateColumn(T3, orderable=False)
    cancel = tables.TemplateColumn(T4, orderable=False)
    #cancel = tables.TemplateColumn(T4, orderable=False, attrs={'td': {'class' : status_cancelled_attr}})
    extend = tables.TemplateColumn(T5, orderable=False)

    class Meta:
        model = MData
        template_name = "django_tables2/bootstrap4.html"
        fields = ("proj", "infrax", "sch_start", "sch_end", "act_start", "act_end", "team", "location", "status", "extensions")
        sequence = ('mnum', '...')

        attrs = { 
            "thead" : { "class" : "text-primary" },
            "class" : "table table-hover table-striped"
        }

        row_attrs = {
            "data-id" : lambda record: record.pk
        }
