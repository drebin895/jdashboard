from django.urls import path
from . import views
from django.conf.urls import url, include
from dashboard.views import MDataListView, MDataEditView, MDataDetailView, MDataViewSet, MDataViewSetToday
from rest_framework import routers
from dashing.utils import router

rest_router = routers.DefaultRouter()
rest_router.register(r'mdata-api', views.MDataViewSet)
rest_router.register(r'mdata-api-today', views.MDataViewSetToday)

urlpatterns = [
   path('', include(rest_router.urls)),
   path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
   path('mdata/', MDataListView.as_view(), name='mdata'),
   path('mdata/<int:pk>/update/', MDataEditView.as_view(), name='mdata-edit'),
   path('mdata/<int:pk>/', MDataDetailView.as_view(), name='mdata-view'),
   url(r'mdata-dashboard/', include(router.urls)),
]
