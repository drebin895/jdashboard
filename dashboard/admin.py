from django.contrib import admin
from .models import MData, MDataResource
from import_export.admin import ImportMixin
from import_export import resources

class MDataAdmin(ImportMixin, admin.ModelAdmin):
    resource_class = MDataResource  

# Register your models here.
admin.site.register(MData, MDataAdmin)
